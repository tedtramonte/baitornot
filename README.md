# Bait Or Not
[![License](https://img.shields.io/badge/license-MIT-brightgreen)](https://choosealicense.com/licenses/mit/)
[![GitLab pipeline](https://img.shields.io/gitlab/pipeline/tedtramonte/baitornot)](https://gitlab.com/tedtramonte/baitornot/-/jobs)
[![GitLab tag](https://img.shields.io/gitlab/v/tag/tedtramonte/baitornot?sort=semver)](https://gitlab.com/tedtramonte/baitornot/-/tags)
[![GitLab release](https://img.shields.io/gitlab/v/release/tedtramonte/baitornot?sort=semver)](https://gitlab.com/tedtramonte/baitornot/-/releases)
[![Production status](https://argocd.tramonte.us/api/badge?name=baitornot)]( https://argocd.tramonte.us/applications/baitornot)

Bait Or Not is a Twitter bot written in Python using the [Tweepy](https://github.com/tweepy/tweepy) Twitter library.

Anyone on Twitter can mention [@baitornot](https://twitter.com/baitornot) in a top-level tweet or a reply to have the bot respond with whether or not the content in question is bait. The bot will also retweet its own reply so that its timeline is a history of what content it has judged.

As of February 2nd, 2023, Twitter has removed free access to its API. I have not been a Twitter user since mid 2021, so I'm no longer interested in actively maintaining this project considering, including bug fixes and feature updates. I just completely detest the service as a whole. I recommend you move your social media activities to federated alternatives such as Mastodon.

## Requirements
- An OCI compliant container engine like Docker or Podman

## Configuration
You will need to [register a new Twitter application](https://developer.twitter.com/en/docs/apps/overview). After your application is approved, you will be able to generate your API credentials.

### Environment Variables
- `ACCESS_TOKEN`: Specify the Twitter account the request is made on behalf of
- `ACCESS_TOKEN_SECRET`: Acts as the password for the `ACCESS_TOKEN`
- `CONSUMER_KEY`: Acts as the username for your Twitter App
- `CONSUMER_SECRET`: Acts as the password for your Twitter App


## Usage
The official Bait Or Not instance is deployed in a Kubernetes cluster using Kustomize. [The official Kustomize repo](https://gitlab.com/tedtramonte/kustomize-baitornot) contains a basic deployment for your own cluster which you can modify to fit your needs.

Using `docker-compose` and a `.env` file, a simple deployment could look something like:
```yaml
services:
  bot:
    image: registry.gitlab.com/tedtramonte/baitornot:latest
    restart: unless-stopped
    environment:
      CONSUMER_KEY: ${CONSUMER_KEY}
      CONSUMER_SECRET: ${CONSUMER_SECRET}
      ACCESS_TOKEN: ${ACCESS_TOKEN}
      ACCESS_TOKEN_SECRET: ${ACCESS_TOKEN_SECRET}
```


## Development
The recommended way to make and test changes to the application is to use [Tilt](https://tilt.dev/) to deploy to a local [k3d](https://k3d.io/) Kubernetes cluster, as this will closely match the official environment.
```shell
k3d cluster create dev --registry-create dev-registry

tilt up
```

Using `docker-compose` and a `.env` file, a simple developer setup could look something like:
```yaml
services:
  bot:
    build: ./
    image: baitornot
    restart: unless-stopped
    environment:
      CONSUMER_KEY: ${CONSUMER_KEY}
      CONSUMER_SECRET: ${CONSUMER_SECRET}
      ACCESS_TOKEN: ${ACCESS_TOKEN}
      ACCESS_TOKEN_SECRET: ${ACCESS_TOKEN_SECRET}
```

[Commitizen](https://github.com/commitizen-tools/commitizen) should be used to update the changelog and create new releases.
```shell
cz bump
git push
git push --tags
```


## Contributing
Merge requests are welcome after opening an issue first.
