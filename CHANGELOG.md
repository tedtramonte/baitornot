## 3.0.0 (2022-01-24)

## 2.0.2 (2020-12-17)

### Fix

- condition checks for bot attribute on status

## 2.0.1 (2020-11-08)

### Fix

- subject uses wrong tweet

## 2.0.0 (2020-06-29)

### Features

- respond to tweets sent while offline

## 1.1.2 (2020-05-29)

### Fix

- log errors and attempt reconnect

## 1.1.1 (2020-05-22)

### Fix

- `attachment_url` syntax

## 1.1.0 (2020-05-21)

### Features

- quote original tweet in reply
- retweet reply

## 1.0.1 (2020-05-20)

### Fix

- compare using `id_str`

## 1.0.0 (2020-05-20)

### Features

- tweet at the bot and the bot will respond "That's bait." or "Not bait."

## 0.1.0 (2020-05-06)
