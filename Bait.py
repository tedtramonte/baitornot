import logging
import os
import random

import tweepy


logging.basicConfig(
    format='%(asctime)s [%(levelname)s] %(message)s', level=logging.INFO)


class BaitOrNot(tweepy.Stream):

    def __init__(self, consumer_key=None, consumer_secret=None, access_token=None, access_token_secret=None):
        super().__init__(consumer_key, consumer_secret, access_token, access_token_secret)
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        self.api = tweepy.API(auth)
        self.user = self.api.verify_credentials()

    def on_error(self, status_code):
        logging.error(f'{status_code}')
        return True  # Attempts to reconnect with backoff

    def on_connect(self):
        try:
            logging.info('Catching up on tweets with no response yet')
            # Get the most recent tweet the bot made
            last_tweet = self.api.user_timeline(count=1)[0]
            logging.info(f'The last tweet responded to was {last_tweet.id}')
            for status in tweepy.Cursor(self.api.mentions_timeline, since_id=last_tweet.id).items():
                if status.in_reply_to_user_id_str != self.user.id_str:
                    logging.info(f'Mentioned in {status.id} while offline')
                    self.on_status(status)
            logging.info('Should be all caught up now')
            logging.info(f'Watching for new tweets @{self.user.screen_name} ({self.user.id})')
        except Exception as e:
            logging.error(e)

    def on_status(self, status):
        try:
            # Set the subject tweet for quoting
            # If the mention is a top level tweet it is thus also the subject
            subject = self.api.get_status(
                status.in_reply_to_status_id) if status.in_reply_to_status_id is not None else status

            conditions = [
                status.in_reply_to_user_id_str != self.user.id_str,
                status.user.id_str != self.user.id_str,
                (subject is not status) and not (self.user.id_str in [
                    mention['id_str'] for mention in subject.entities['user_mentions']]),
            ]

            if all(conditions):
                if self.user.id_str in [mention['id_str'] for mention in status.entities['user_mentions']]:
                    # Set the tweet message
                    # TODO: Make the determination more scientific or something
                    if random.randint(0, 100) >= 20:
                        msg = 'That\'s bait.'
                    else:
                        msg = 'Not bait.'

                    reply = self.api.update_status(
                        status=msg,
                        in_reply_to_status_id=status.id,
                        auto_populate_reply_metadata=True,
                        attachment_url=f'https://twitter.com/{subject.user.id}/status/{subject.id}'
                    )
                    self.api.retweet(reply.id)
                    logging.info(
                        f'{status.user.id} wants {status.in_reply_to_status_id or status.id} to be judged - {msg}')
            else:
                logging.info(f'{status.id} did not meet the conditions')
        except Exception as e:
            raise(e)


credentials = {
    'consumer_key': os.getenv('CONSUMER_KEY'),
    'consumer_secret': os.getenv('CONSUMER_SECRET'),
    'access_token': os.getenv('ACCESS_TOKEN'),
    'access_token_secret': os.getenv('ACCESS_TOKEN_SECRET'),
}
bot = BaitOrNot(
    credentials['consumer_key'],
    credentials['consumer_secret'],
    credentials['access_token'],
    credentials['access_token_secret']
)

try:
    bot.filter(track=[bot.user.screen_name])
except Exception as e:
    logging.error(e)
