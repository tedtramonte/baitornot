FROM python:3.10.7-alpine@sha256:486782edd7f7363ffdc256fc952265a5cbe0a6e047a6a1ff51871d2cdb665351
ENV PYTHONBUFFERED 1
WORKDIR /bot
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . ./
CMD ["python", "-u", "Bait.py"]
